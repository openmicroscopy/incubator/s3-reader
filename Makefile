# The intent of this Makefile is to provide a
# CWL-esque sequence of calls to various Docker
# files necessary to generate and process example
# data.

DATA ?= $(PWD)/data

PUBLIC ?= $(DATA)/public

FAKE ?= image.fake

FOV ?= primary_image-fov_000.json

WRITER_IMAGE ?= registry.gitlab.com/openmicroscopy/incubator/spacetx-fov-writer

help:
	@printf "    -------------------------------------------\n"
	@printf "    Main targets:                              \n"
	@printf "    %-16s  print this text (default)           \n" help
	@printf "    %-16s  run steps necessary for full demo   \n" all
	@printf "    %-16s  generate $(FAKE) in $(PUBLIC)       \n" fake
	@printf "    %-16s  generate $(FOV) in $(PUBLIC)        \n" fov
	@printf "    %-16s  launch minio server                 \n" minio
	@printf "    %-16s  run starfish script                 \n" run
	@printf "    %-16s  delete generated files              \n" clean
	@printf "    -------------------------------------------\n"
	@printf "    Additional targets:                        \n"
	@printf "    %-16s  pull all docker images              \n" pull

all: fov minio run

fake: $(PUBLIC)/$(FAKE) $(PUBLIC)/$(FAKE).ini

$(PUBLIC)/$(FAKE).ini: $(PUBLIC)
	echo sizeX=16 > $(PUBLIC)/$(FAKE).ini
	echo sizeY=16 >> $(PUBLIC)/$(FAKE).ini

$(PUBLIC)/$(FAKE): $(PUBLIC)
	touch $(PUBLIC)/$(FAKE)

$(PUBLIC):
	mkdir -p $(PUBLIC)

fov: $(PUBLIC)/$(FOV)

$(PUBLIC)/$(FOV): $(PUBLIC)/$(FAKE).ini
	docker run --rm \
		-v $(PUBLIC):/data \
		$(WRITER_IMAGE) \
		-o /data/fov_000 \
		/data/$(FAKE).ini

minio:
	export DATA=$(DATA); docker-compose up -d mc

run: $(PUBLIC)/$(FOV)
	docker-compose run --rm starfish 2>&1 | tee run

clean:
	docker-compose down
	rm -rf $(PUBLIC)
	rm -f run

pull:
	docker-compose pull
	docker pull $(WRITER_IMAGE)

.PHONY: help all fake fov minio clean pull
