#!/usr/bin/env python
# coding: utf-8
### Loading the data into Starfish
from starfish.experiment.experiment import Experiment
from starfish.types import Indices
# s3://public.localhost:9000/fov_000/hybridization-fov000.json
experiment = Experiment.from_json(
    "http://nginx:9000/public/fov_000/experiment.json",
    strict=True)  # soon: allow_caching=False
    # see https://github.com/spacetx/starfish/pull/645
image = experiment.fov().primary_image
image.show_stack({Indices.CH: 0})
print(image)
print("Done.")
